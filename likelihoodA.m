function phi = likelihoodA(theta)
%% Calculates negative logarithm of likelihood function,
%% this function is minimized by parest.m.
%% Gradients are calculated by adjoint method.
  global mod_ meas_ obj_ data
  %% data.theta contains all parameters, not only those which are estimated!
  data.theta = mod_.param;
  %% set the model parameters; note the log transformation
  data.theta(obj_.estflag) = theta;
  trind =  obj_.estflag(obj_.logtr);
  data.theta(trind) = exp(theta(obj_.logtr));

  %% set the state ICs that are parameters
  mod_.ic(mod_.stateic_par) = data.theta(mod_.par_stateic);

  %% Set options for integrator.
  options = CVodeSetOptions ('RelTol', sqrt (eps),
			   'AbsTol', sqrt (eps));

  %% Allocate storage and set initial conditions for integrator.
  if (~isfield(mod_,'cvodesfcn'))
  CVodeMalloc ('parest:oderhs', 0, mod_.ic, options, data);
  else
  CVodeMalloc (mod_.cvodesfcn, 0, mod_.ic, options, data);
  end

  tmeas = meas_.time;
  t0 = 0;
  t1 = tmeas(end);
  nmeas = size(meas_.data, 1);
  ntimes = size(meas_.data, 2);
  nx = numel (mod_.ic);
  npar = numel(obj_.estflag);

  x = zeros (nx,ntimes);
  mu = zeros (nx,ntimes);

  x(:,1) = vec(mod_.ic);

  %% Integrate forward, saving intermediate points for calculation of
  %% the objective function.
  iout = 1;
  measic = 0;
  if (tmeas(1) == 0)  measic = 1; endif
  tout = tmeas(1+measic);
  nsteps = ntimes - measic + 1;

  while (true)
  [status, t, x_step] = CVode (tout, 'Normal');
  if (status == 0)
    iout++;
    x(:,iout) = x_step;
  else
		warning ('parest: CVode failed with status = %d', status);
		break;
  endif
  if (iout == nsteps)
    break;
  endif
  tout = tmeas(iout + measic);
  endwhile

  if(status < 0)
	CVodeFree ();
	phi = realmax;
	return;
  endif

  %% Allocate memory for checkpointing of forward solution for
  %% backward integration of adjoint problem.
  CVadjMalloc (150, 'Hermite');

  %%compute residual and objective function for these param values
  %%compute gradient, here NO Hessian approximation; have to store in common
  if (isfield(meas_,'statefcn'))
		% if user provides a measurement state function
		statefcnExists = true;
		resid = zeros(nmeas,ntimes);
		for t_ind = 1:ntimes	%loop over all time points
			resid(:,t_ind) = meas_.data(:,t_ind) - meas_.statefcn(x(:,t_ind));
		endfor
	else
		% if user provides only vector of measured states
		statefcnExists = false;
		resid = meas_.data - x(meas_.states,:);
		IdentityMatrix = eye(nx);
		dhdx = IdentityMatrix(meas_.states,:);
  endif
  obj_.resid = resid;
  %% find and delete bad data
  resid(find(~finite(resid))) = 0;

  phi = trace(resid'*meas_.weight*resid)

  %% Free storage for the integration.
  %CVodeFree ();

  %% Set up reverse integration.  Must be done after the forward
  %% integration is complete.

  %% Initial condition for the backward quadrature.
  w = zeros (1, npar);

  %% Set options for the backward integration of the adjoint equation and
  %% quadrature.
  optionsb = CVodeSetOptions ('Quadratures', 'on',
			    'QuadRhsFn', 'likelihoodA:sensrhsA',
			    'QuadInitcond', w,
			    'QuadErrControl', "on",
			    'QuadRelTol', sqrt (eps),
			    'QuadAbsTol', sqrt (eps),
			    'RelTol', sqrt (eps),
			    'AbsTol', sqrt (eps));

  %% Initial condition for the backward integration of the adjoint
  %% equations
	if (statefcnExists)
		% Jacobian at last time point if statefcn provided by user
		sqdhdx = meas_.dstatedx(x(:,end));
		%% find and delete bad data
		sqdhdx(find(~finite(meas_.data(:,end))),:) = 0;
	else
		% Jacobian at last time point if only measurement provided by user
		sqdhdx = dhdx;
		%% find and delete bad data
		sqdhdx(find(~finite(meas_.data(:,end))),:) = 0;
	endif
  mu_init = -2*( resid(:,end)'*meas_.weight*sqdhdx )';
  mu(:,end) = mu_init;
  %% Allocate storage and set initial conditions for integrator.
  CVodeMallocB ('likelihoodA:adjrhs', t1, mu_init, optionsb);

  %% Integrate backward, accumulating in w the gradient of phi, which is
  %% the least-squares objective \sum resid*meas_.weight*resid.
  for (i = ntimes-1:-1:1)
	tinit = tmeas(i+1);
	tout = tmeas(i);
	CVodeReInitB ('likelihoodA:adjrhs', tinit, mu_init, optionsb);
    while (true)
	[status, t_step, mu_step, w_step] = CVodeB (tout, "Normal");
    	if (status < 0)
		warning ('parest: CVodeB failed with status = %d', status);
		break;
    	endif
	%% We are integrating backward, and we are storing the results in
	%% reverse order too.
    	if (status == 0 || (status == 1 && t_step == 0))
	if (statefcnExists)
		% Jacobian at time point i if statefcn provided by user
		sqdhdx = meas_.dstatedx(x(:,i));
		%% find and delete bad data
		sqdhdx(find(~finite(meas_.data(:,i))),:) = 0;
	else
		% Jacobian at time point i if only measurement provided by user
		sqdhdx = dhdx;
		%% find and delete bad data
		sqdhdx(find(~finite(meas_.data(:,i))),:) = 0;
	endif
      	mu_init = mu_step - 2*(resid(:,i)'*meas_.weight*sqdhdx )';
      	mu(:,i) = mu_init;
      	w = w + w_step';
      	break;
    endif
    endwhile
  endfor

if( tmeas(1) > 0) %% integrate till t = 0
  CVodeReInitB ('likelihoodA:adjrhs', tmeas(1), mu_init, optionsb);
  [status, t_s, mu_0, w_0] = CVodeB (t0, 'Normal');
    	if (status ~= 0)
		warning ('parest: CVodeB failed with status = %d', status);
		break;
    	else
      w = w + w_step';
   endif
else
  mu_0 = mu_init;
endif

%% Free storage for the integration.
CVodeFree ();
%CVadjFree ();

    %% gradient with respect to untransformed parameters
    %% Eqn (3.19) of CVodeS user guide
    %% sx0 = 0 for parameters that are not state IC
    %% sx0 = 1 for parameters that are state IC
    grad = w + mu_0'*mod_.sx0;
    obj_.grad = grad;
    %% gradient with respect to log transformed parameters
    %% this is the optimizer's gradient (see gradi function)
    scale = ones(npar,1);
    scale(trind) = data.theta(trind);
    obj_.gradlogtr = obj_.grad*diag(scale);
    %% values of x at tmeas
    obj_.x = x;
    obj_.mu = mu;

endfunction %LikelihoodA
% END OF FUNCTION LIKELIHOODA.m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [qbd, flag, new_data] = sensrhsA (t, x, mu, data)
  global mod_ obj_
  % Backward quadrature function use to compute gradient of objective
  % function.  In this problem, it computes Equation 3.19 from the CVodes
  % user guide:
  %   dg/dp(t1) = mu(t0)s(t0) + g_p(t1) + \int_t0^t1 mu'*f_p
  % 
  % compute the integral by integrating
  %    dw/dt = - mu' f_p
  %    w(t1) = 0
  % 
  %  backward over the time horizon.
  dfdp = mod_.dodedp(x, t, data.theta);
  dfdp = dfdp(:,obj_.estflag);
  qbd = -(mu'*dfdp)';
  flag = 0;
  new_data = [];
endfunction %sensrhsA

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [mudot, flag, new_data] = adjrhs (t, x, mu, data)
  global mod_
  % RHS of adjoint system.  In this problem, it computes Equation 3.20
  % from the CVodes user guide:
  %    mudot = - f_x'*mu
  %    mu(t1) = g_x' at t=t1
  mudot = -mod_.dodedx(x, t, data.theta)'*mu;
  flag = 0;
  new_data = [];
endfunction %adjrhs
