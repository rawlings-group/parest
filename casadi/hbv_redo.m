%%
%% jbr, elh,  11/28/01
%%
%% A = cccDNA, B = rcDNA, C = env
%%
%% Reactions
%% 1: A -> A + B
%% 2: B -> A
%% 3: A -> A + C
%% 4: A -> 0 (degradation)
%% 5: C -> 0 (degradation)
%% 6: B + C -> 0 (secreted virus)

%% converted to use parest, jbr, 4/24/2007
%% converted to use paresto (casadi), jbr, 4/13/2018
%%
%% reduced hbv model
%%

more off

model=struct;
model.nlp_solver_options.ipopt.linear_solver = 'ma27';
%model.nlp_solver_options.ipopt.print_level = 0;
%model.nlp_solver_options.print_time = false;
%model.transcription = 'shooting';

model.x = {'ca', 'cb', 'cc'};
model.p = {'k1', 'k2', 'k3', 'k4', 'k5', 'k6', 'wa', 'wb', 'wc'};
model.d = {'ma', 'mb', 'mc'};

function rhs = hbv_rxs(t, y, p)
  kr = [ 10.^(p.k1) + p.k4, ...
	 10.^(p.k2) * p.k4, ...
	 p.k3, ...
	 p.k4, ...
	 p.k3 / 10.^p.k5, ...
	 10.^p.k6 ];
  rhs = {kr(2)*y.cb - kr(4)*y.ca, ...
         kr(1)*y.ca - kr(2)*y.cb - kr(6)*y.cb*y.cc, ...
         kr(3)*y.ca - kr(5)*y.cc - kr(6)*y.cb*y.cc};
end%function

model.ode = @hbv_rxs;
model.lsq = @(t, y, p) {p.wa*(y.ca-y.ma), p.wb*(y.cb-y.mb), p.wc*(y.cc-y.mc)};

%% Set the reaction rate constant vector kr; use log transformation
krac = [2; 0.025; 1000; 0.25; 1.9985; 7.5E-6];
thetaac = [log10(krac(1)-krac(4)); ...
           log10(krac(2)/krac(4)); ...
           krac(3); ...
	   krac(4); ...
	   log10(krac(3)/krac(5));
           log10(krac(6))];
p.k1 = thetaac(1); p.k2=thetaac(2); p.k3 = thetaac(3);
p.k4 = thetaac(4); p.k5 = thetaac(5); p.k6=thetaac(6);

%% output times
tfinal = 100;
ndata = 51;
tdata = linspace(0, tfinal, ndata)';
model.tout = tdata;

%% measurement weights
mweight = sqrt([1; 1e-2; 1e-4]);
p.wa = mweight(1); p.wb = mweight(2); p.wc = mweight(3);
p_ac = [thetaac; mweight];

pe = paresto(model);

%% Set the initial condition
small = 0;
x0_ac  = [1; small; small];

y_ac = pe.simulate(zeros(3, 1), x0_ac, p_ac);

randn('seed',1);
R = diag([0.1 0.1 0.1])^2;
noise = sqrt(R)*randn(size(y_ac));
%% proportional error
y_noisy = y_ac .* (1 + noise);
y_noisy = max(y_noisy, 0);

%% initialize all parameters
%% index of estimated parameters
ind = [1, 2, 5, 6];
del = 1;
thetaic = thetaac;
%% perturb the initial parameter estimates
thetaic(ind) += 0.75*del;
theta0 = [thetaic; mweight; x0_ac];
lbtheta = theta0;
ubtheta = theta0;

%% loosen bounds bounds on estimated parameters
lbtheta(ind) += -del;
ubtheta(ind) += del;

[est, y, p] = pe.optimize(y_noisy, theta0, lbtheta, ubtheta);

theta_conf = pe.confidence(est, ind, 0.95);

disp('Optimal parameters and confidence intervals')
[est.theta(ind), theta_conf]

%% initial guess
figure(1)
plot(tdata, y_ac, tdata, y_noisy, 'o')

%% optimal fit
figure(2)
plot(tdata, est.x, tdata, y_noisy, 'o')

figure(3)
semilogy(tdata, est.x, tdata, y_noisy, 'o')

%% diagnose problems

r_ac = diag(mweight)*(y_ac-y_noisy);
phi_ac = sum(diag(r_ac'*r_ac))
rfit = diag(mweight)*(est.x-y_noisy);
phi_fit = sum(diag(rfit'*rfit))


