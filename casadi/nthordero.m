%%
%% jbr,  4/8/18
%%


model = struct;
model.x = {'ca'};
model.p = {'k', 'n'};

model.d = {'m_ca'};

model.nlp_solver_options.ipopt.linear_solver = 'ma27';
model.nlp_solver_options.ipopt.print_level = 0;
model.nlp_solver_options.print_time = false;


model.ode = @(t, y, p) {-p.k*y.ca^p.n};

model.lsq = @(t, y, p) {y.ca-y.m_ca};

tfinal = 5;
nts    = 100;
tout = linspace(0,tfinal,nts);
model.tout = tout;

pe = paresto(model);
kac   = 0.5;
ca0ac = 2;
nac   = 2.5;
thetaac = [kac; ca0ac; nac];

x_ac0 = ca0ac;
p_ac = [kac; nac];

y_ac = pe.simulate(0, x_ac0, p_ac);

%% add measurement noise
measvar = 1e-2;
measstddev = sqrt(measvar);
randn('seed',0);
noise = measstddev*randn(1,nts);

y_noisy = y_ac + noise;


% Initial guess, upper and lower bounds for the estimated parameters
theta0  = [0.5; 2.5; 2.0];
lbtheta = 0.5*[p_ac; x_ac0(:)];
ubtheta = 1.5*[p_ac; x_ac0(:)];
est_ind = find(lbtheta~=ubtheta); % index of estimated parameters

% Estimate parameters
[est, y, p] = pe.optimize(y_noisy, theta0, lbtheta, ubtheta);

% Also calculate confidence intervals with 95 % confidence
theta_conf = pe.confidence(est, est_ind, 0.95);

disp('Estimated parameters and confidence intervals')
[est.theta(est_ind), theta_conf]

% Plot simulated measurement values
plot(tout, y_noisy, 'ro', tout, y.ca)

table  = [tout; y_noisy; y.ca; y_ac]';
save nthordero.dat table;
