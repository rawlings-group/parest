%
% We have the reduced ODE model
% dot(VR) = Qf
% dot(eps2) = Qf*cBf/(1 + k (nA0-nBadded+eps2)/(nBadded-2*eps2))
%
%
% with:
% States: x = [VR, eps2]  volume and extent of 2nd reaction
% Qf: Volumetric flowrate of base
% cBf: Feed concentration of B
%
% Initial conditions: x(0) = [VR0, 0]
% Unknown parameters: k (= k1/k2), n_A0
% Output function: y = nC/(nC + 2*nD) = 1/(1+2*nD/nC)
%
% jbr, Joel Andersson, 4/18/2018
%

% Model
model = struct;
model.transcription = 'shooting';
model.nlp_solver_options.ipopt.linear_solver = 'ma27';
model.x = {'VR', 'eps2'};
model.p = {'k', 'nA0', 'cBf', 'VR0'};

% Dependent variables with definitions
model.y = {'lc'};

function retval = lcmeas(t, v, p)
  Badded = (v.VR - p.VR0)*p.cBf;
  nD = v.eps2;
  nC = Badded - 2*nD;
  retval = { 1 / (1 + 2*nD/max(nC, 1e-6))}; % avoid divide-by-zero
end%function

model.h = @lcmeas;

% Data and measurements
model.d = {'Qf', 'lc_m'};

function xdot = reduced_model(t, v, p)
  Badded = max((v.VR - p.VR0)*p.cBf, 1e-6); % avoid divide-by-zero
  deps2dt = v.Qf*p.cBf / (1. + p.k*(p.nA0 - Badded + v.eps2)/(Badded - 2*v.eps2));
  xdot = {v.Qf, deps2dt};
end%function

% ODE right-hand-side
model.ode = @reduced_model;

% Relative least squares objective function
model.lsq = @(t, y, p) {y.lc_m/y.lc - 1};

% Load data
teaf      = 0.00721;
teaden    = 0.728;
flow = load('flow.dat');
lc = load('lc.dat');
tQf  = [0. ; flow(:,1)];
Qf = [0. ; flow(:,2)./teaden];
tlc    = lc(:,1);
lc = lc(:,2);

% Get all time points occuring in either tlc or tflow

%% Grid used in Book
## ntimes    = 200;
## tlin = linspace (0, tQf(end), ntimes)';
## [tout,~,ic] = unique([tQf; tlc; tlin]);
## Qf_ind = ic(1:numel(tQf));
## lc_ind = ic(numel(tQf)+1:numel(tQf)+numel(tlc));

## faster grid; lose a little resolution in n_B(t) plot
[tout,~,ic] = unique([tQf; tlc]);
Qf_ind = ic(1:numel(tQf));
lc_ind = ic(numel(tQf)+1:end);

%% Interpolate lcmeas and Qf to this new grid
Qf = interp1(tQf, Qf, tout, 'previous');
lc_m = interp1(tlc, lc, tout, 'previous');

% Replace NaNs with zeros
Qf(isnan(Qf)) = 0.;
lc_m(isnan(lc_m)) = 0.;

% Initial volume
VR0 = 2370;
%% optimal values
vrdiclo     =  2.3497;
k1k2ratio   =  2;

% Options
model.tout = tout';
model.lsq_ind = lc_ind'; % only include lc_ind in objective

% Create a paresto instance
pe = paresto(model);

% Initial guess, upper and lower bounds for the estimated parameters
p0 =  [k1k2ratio; vrdiclo; teaf; VR0];
lbp = p0;
est_ind = [1, 2]; % index of parameters to estimate; the rest are constants
lbp(est_ind) = 0.5*p0(est_ind);
ubp = p0
%ubp(est_ind) = 2.0*p0(est_ind);
ubp(est_ind) = 1.5*p0(est_ind);
ic0 = [VR0; 0];
lbic = ic0;
ubic = ic0;
lbtheta = [lbp; lbic];
ubtheta = [ubp; ubic];
theta0 = [p0; ic0];

%y_ac = pe.simulate([Qf'; lc_m'], [VR0; 0], p0);

%% Estimate parameters
more off

[est, v, p] = pe.optimize([Qf'; lc_m'], theta0, lbtheta, ubtheta);

% Also calculate confidence intervals with 95 % confidence
theta_conf = pe.confidence(est, est_ind, 0.95);

disp('Estimated parameters and confidence intervals')
[est.theta(est_ind), theta_conf]

% compute the rest of the states from the reduced model

Badded = (v.VR - p.VR0)*p.cBf;
nD = v.eps2;
nC = Badded - 2*nD;
nB = zeros(size(Badded));
eps1 = Badded-v.eps2;
nA = p.nA0 - Badded + v.eps2;
deps2dt = v.Qf*p.cBf / (1. + p.k*(p.nA0 - Badded + v.eps2)/(Badded - 2*v.eps2));

%plot(model.tout, v.lc, tlc, lc, 'o')

figure(1)
subplot(2,2,1)
hold on
plot(model.tout, nA)
plot(model.tout, nC)
plot(model.tout, nD)
legend({'n_A', 'n_C', 'n_D'});
xlabel('time (min)')
ylabel('Amount of substance (kmol)')
title('Amount of substance of species A, C and D versus time')

subplot(2,2,2)
hold on
plot(model.tout, nB)
legend({'n_B'});
xlabel('time (min)')
ylabel('Amount of substance (kmol)')
title('Amount of substance of species B versus time')

subplot(2,2,3)
stairs(model.tout, v.Qf)
xlabel('time (min)')
ylabel('flowrate (kg/min)')
title('Base addition rate')

subplot(2,2,4)
hold on
plot(model.tout, v.lc)
plot(tlc, lc, 'o')
ylim([0, 2*max(lc)])
legend({'model', 'measurement'});
xlabel('time (min)')
title('LC measurement')

figure(2)
plot(model.tout, [deps2dt; v.eps2])

