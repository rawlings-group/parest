%% Exercise 9.11, part a, using only first experiment
%use_first = true; use_second = false;

%% Exercise 9.11, part b, using only second experiment
%use_first = false; use_second = true;

%% Exercise 9.11, part c, using both experiments
use_first = true; use_second = true;

% Number of experiments
nsets = use_first + use_second;

% Model
model = struct;
model.nlp_solver_options.ipopt.linear_solver = 'ma27';
%model.nlp_solver_options.ipopt.print_level = 0;
%model.nlp_solver_options.print_time = false;

model.x = {'ca', 'cb'};
model.p = {'k', 'n', 'm'};

% Measurements of the state enter the problem as data
model.d = {'m_ca', 'm_cb'};

% ODE right-hand-side
model.ode = @(t, y, p) {-p.k * y.ca^p.n * y.cb^p.m, ...
                        -p.k * y.ca^p.n * y.cb^p.m};

% Least squares objective function
model.lsq = @(t, y, p) {y.ca-y.m_ca, y.cb-y.m_cb};

% Options
tfinal = 5;
nts    = 100;
tout = linspace(0,tfinal,nts);
model.tout = tout;
model.nsets = nsets;

% Create a paresto instance
pe = paresto(model);

% True parameter values (not known to optimizer)
kac   = 0.1;
nac   = 2;
mac   = 1;
p_ac = [kac; nac; mac];

% Actual values for ca0 and cb0
x_ac = [];
if use_first
  x_ac = [x_ac, [2; 4]];
end
if use_second
  x_ac = [x_ac, [5; 3]];
end

% Simulate
y_ac = pe.simulate([0;0], x_ac, p_ac);

% set seed for "reproducible" random numbers
randn('seed',0);

% add measurement noise
measvar = 1e-2;
measstddev = sqrt(measvar);
noise = measstddev*randn(2, nts, nsets);
y_noisy = y_ac + noise;

% Plot simulated measurement values
figure(1)
clf;
for e=1:nsets
  subplot(nsets,2,e)
  hold on;
  plot(tout, y_noisy(1,:,e), 'ro');
  plot(tout, y_noisy(2,:,e), 'bo');
  legend({'c_A', 'c_B'})
  title(sprintf('Measured values, experiment %d', e));
  xlabel('t (min)')
  ylabel('c (mol/L)')
end

% Initial guess, upper and lower bounds for the estimated parameters
theta0 = 1.1*[p_ac; x_ac(:)];
lbtheta = 0.5*[p_ac; x_ac(:)];
ubtheta = 1.5*[p_ac; x_ac(:)];

% Estimate parameters
[est,y,p] = pe.optimize(y_noisy, theta0, lbtheta, ubtheta);
est.theta
est.d2f_dtheta2

% Also calculate confidence intervals with 95 % confidence
theta_conf = pe.confidence(est, 1:numel(theta0), 0.95)

disp('Confidence interval')
theta_conf

% Plot optimized trajectories
for i=1:nsets
  subplot(nsets,2,nsets+i)
  plot(tout, y.ca(:,:,i), '*');
  hold on;
  plot(tout, y.cb(:,:,i), '*');
  plot(tout, y_noisy(:,:,i), 'o');
  legend({'c_A (estimated)', 'c_B (estimated)', 'c_A (measured)', 'c_B (measured)'})
  title('Optimized values')
  xlabel('t (min)')
  ylabel('c (mol/L)')
end
