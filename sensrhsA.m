function [qbd, flag, new_data] = sensrhsA (t, x, mu, data)
% Backward quadrature function use to compute gradient of objective
% function.  In this problem, it computes Equation 3.19 from the CVodes
% user guide:
%   dg/dp(t1) = mu(t0)s(t0) + g_p(t1) + \int_t0^t1 mu'*f_p
% 
% compute the integral by integrating
%    dw/dt = - mu' f_p
%    w(t1) = 0
% 
%  backward over the time horizon.
global mod_ obj_
  dfdp = mod_.dodedp(x, t, data.theta);
  dfdp = dfdp(:,obj_.estflag);
  qbd = -(mu'*dfdp)';
  flag = 0;
  new_data = [];
testsensrhsA = 0
endfunction
