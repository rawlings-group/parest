%% Copyright (C) 2007, James B. Rawlings and John G. Ekerdt
%%
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation; either version 2, or (at
%% your option) any later version.
%%
%% This program is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program; see the file COPYING.  If not, write to
%% the Free Software Foundation, 59 Temple Place - Suite 330, Boston,
%% MA 02111-1307, USA.

%%
%% jbr, 1/22/2007
%%
%% Solve the deterministic model for the HBV system
%%
%% A = cccDNA, B = rcDNA, C = env
close all
clear all
global data model

%% Set the reaction rate constant vector kr
krac = [2 0.025 1000 0.25 1.9985 7.5E-6]';
tfinal = 110;
nplot = 100;

function [xdot] = hbv_deriv_cvode(x, t, theta) % rhs of ode = f(x,t,p)
  kr = theta;
  xdot= [kr(2)*x(2)-kr(4)*x(1);
         kr(1)*x(1)-kr(2)*x(2)-kr(6)*x(2)*x(3);
         kr(3)*x(1)-kr(5)*x(3)-kr(6)*x(2)*x(3)];
  flag = 0;
  new_data = [];
endfunction

function xdot = dfdtheta(x, t, theta) % df/dp
  kr = theta;
  xdot= [0,x(2),0,-x(1),0,0;
         x(1),-x(2),0,0,0,-x(2)*x(3);
         0,0,x(1),0,-x(3),-x(2)*x(3)];
endfunction

function xdot = dfdx(x, t, theta) % df/dx
  kr = theta;
  xdot= [-kr(4),kr(2),0;
         kr(1),-kr(2)-kr(6)*x(3),-kr(6)*x(2);
         kr(3),-kr(6)*x(3),-kr(5)-kr(6)*x(2)];
endfunction

function retval = hbv_fixme(x,t)
  global data
  [xdot] = hbv_deriv_cvode(x, t, data.theta);
  retval = xdot;
endfunction

function retval = h(x)
  %% something simple, measure the state
  retval = x;
endfunction

function retval = dh(x)
  %% dh(x)/dx;
  nstate = columns(x);
  E = eye(nstate);
  retval = E;
  %ntime  = rows(x);
  %a = reshape(E, [1, size(E)]);
  %retval = repmat(a, [ntime, 1, 1]);
endfunction

% rhs of ode and its derivatives
model.odefcn  = @hbv_deriv_cvode;
model.dodedx  = @dfdx; %
model.dodedp  = @dfdtheta; %

model.tplot   = linspace(0,tfinal,nplot);
% parameters of model
model.param   = krac;
data.theta    = krac;

%% which  model parameters are initial conditions of which model states
model.par_stateic = [];
model.stateic_par = [];

%% set initial conditions that are not model parameters;
model.ic  = [1 0 0]';

%measure.statefcn = @h;
%measure.dstatedx = @dh;
%measure.states = [1 2 3];
measure.time = linspace(0, 100, 51);
measure.weight = diag([1, 1, 1]);%diag([1, 1e-2, 1e-4]);
measure.alpha = 0.95;

objective.estflag = [1 2 5];
objective.paric   = 1.1*krac(objective.estflag);
objective.logtr   = logical(ones(numel(objective.estflag),1)); % use log transformation of parameters?
objective.parlb   = objective.paric*exp(-1.5); %lower bound
objective.parub   = objective.paric*exp(1.5);  % upper bound
%objective.parub(1) = objective.parlb(1)*0.7;
%objective.sensitivityAdjointMethod = true; %optional

%%create the measurement data by solving the model and adding noise
%opts = odeset ('AbsTol', sqrt (eps), 'RelTol', sqrt (eps));
%[tsolver, xac] = ode15s(@hbv_fixme, measure.time, model.ic, opts);
xac = lsode(@hbv_fixme, model.ic,measure.time);
randn("seed",1)
R = diag([0.1 0.1 0.1])^2;
noise = randn(size(xac))*sqrt(R*1000);

%% additive or proportional error
meas = xac;
meas = xac+noise;
%meas = xac.*(1 + noise);
measure.data = max(meas,0)';
% simulate missing data
measure.data(1,4) = NaN;
measure.data(2,12) = NaN;

page_screen_output(1)
page_output_immediately(0)

%% estimate the parameters
estimates = parest(model,measure, objective);

Var = estimates.var
phi = estimates.obj
par = [estimates.parest]
bb =[estimates.bbox]
krac

%  
%  for dummycounter = 1:5
%  dummycounter
%  estimates.var
%  measure.weight = inv(estimates.var); %XXX
%  %objective.paric = estimates.parest;
%  estimates = parest(model,measure, objective);
%  endfor
%  
%  Var = estimates.var
%  par = estimates.parest
%  krac
%  
%  
%  
%  % %%
% %% estimate the parameters from the simulated data
% %%
% S = zeros (ns*ndata, np);
% w = kron (ones (ndata, 1), diag (mweight));  % Create a column vector of

% function phi = model(theta)
%   global tdata meas mweight x xdot sx ns ndata np grad S w rs kr resid
%   x0 = [1 0 0]';
%   sx0 = zeros(length(x0), length(theta));
%   [x, xdot, sx, sxdot] = ddasac('hbv_deriv', x0, theta, sx0, tdata);
%   resid = (meas - x);
%   %% stack the residuals in a column vector
%   tmp = resid';
%   rs = tmp(:);
%   phi = rs'*(w.*rs);
%   for i = 1:np
%     %% sensitivity matrix
%     tmp = nth (sx, i)';
%     S(:,i) = tmp(:);
%   endfor
%   grad = -2 * (S'*(w.*rs));
% endfunction
% function retval = gradient(theta)
%   global grad
%   %%
%   %% npsol should have just called model with same value of theta and
%   %% model has commputed grad.
%   %% So pull the gradient out of the global variables
%   retval = grad;
% endfunction
% %%
% %% optimize
% %%
% lb     = thetaac - 1.5;
% ub     = thetaac + 1.5;
% %  more off;
% npsol_options ('major print level', 10);
% %  npsol_options ('minor print level', 10);
% %%
% %% randomc initial guess for parameters
% %%
% rand('seed',0);
% theta0 = thetaac + (rand(np,1)-0.5);
% %%
% %% very flat surface; increase the major iteration from 50 to 100
% %% to try to terminate consistently on different computing hardware
% %%
% npsol_options ('major iteration limit', 100);
% [theta, obj, info, lambda] = npsol (theta0, ['model'; 'gradient'],lb,ub);

% nplot = 200;
% tplot = linspace(0, tfinal, nplot)';
% x0  = [1 0 0]';
% sx0 = zeros(ns, np);
% %%
% %% initial guess fit
% %%
% xplot0 = ddasac('hbv_deriv', x0, theta0, sx0, tplot);
% %%
% %% best model fit
% %%
% xplot1 = ddasac('hbv_deriv', x0, theta, sx0, tplot);
% %%
% %% compute the Gauss-Newton approximation to the Hessian
% %%
% phi = model(theta);
% grad = gradient(theta);
% X = zeros (ns*ndata, np);
% Y = X;
% w = kron (ones (ndata, 1), diag (mweight));  % Create a column vector of
% 					     % diag (W) 
% for i = 1:np                         % repeated nt times.
%   tmp = nth (sx, i)';
%   xx = tmp(:);
%   Y(:,i) = w .* xx;                  % Scale the rows of S(t) by diag (W)
%   X(:,i) = xx;
% endfor
% Hgn = 2 * (X' * Y);

% alpha = 0.95;
% Fstat = finv(alpha,np,ndata-np);
% samplevar = phi/(ndata-np);

% inva = inv(Hgn);
% bbox = sqrt(2*np*Fstat*samplevar*diag(inva));

% %%
% %% show perturbation in least determined parameter direction
% %%
% [u,lam]=eig(Hgn);
% %%
% %% watch out, eigenvalues may be unordered; 
% %% run sort so i(1) will be the index of the smallest eigenvalue;
% %%  still have a +/- sign ambiguity on u(:,i(1)), but thetap with either
% %%  sign on u(:,i(1)) should give the same fit to the data
% %%
% [s,i] = sort(diag(lam));
% thetap = theta + u(:,i(1));
% xplotp = ddasac('hbv_deriv', x0, thetap, sx0, tplot);

% store1 = [tplot xplot1 xplot0 xplotp];
% save -ascii hbv_det_fit.dat store1;
% store2 = [tdata meas];
% save -ascii hbv_det_data.dat store2;

% if (~ strcmp (getenv ('OMIT_PLOTS'), 'true')) %% PLOTTING
% subplot (3, 2, 1);
% plot (store2(:,1), store2(:,2), '+', store1(:,1), store1(:,[2,5]));
% axis ([0, 100, 0, 60]);
% %% TITLE hbv_det_cccdna

% subplot (3, 2, 3);
% plot (store2(:,1), store2(:,3), '+', store1(:,1), store1(:,[3,6]));
% axis ([0, 100, 0, 600]);
% %% TITLE hbv_det_rcdna

% subplot (3, 2, 5);
% plot (store2(:,1), store2(:,4), '+', store1(:,1), store1(:,[4,7]));
% axis ([0, 100, 0, 30000]);
% %% TITLE hbv_det_env

% subplot (3, 2, 2);
% plot (store1(:,1), store1(:,[2,8]));
% axis ([0, 100, 0, 60]);
% %% TITLE hbv_det_cccdnap

% subplot (3, 2, 4);
% plot (store1(:,1), store1(:,[3,9]));
% axis ([0, 100, 0, 600]);
% %% TITLE hbv_det_rcndap

% subplot (3, 2, 6);
% plot (store1(:,1), store1(:,[4,10]));
% axis ([0, 100, 0, 30000]);
% %% TITLE hbv_det_envp
% endif %% PLOTTING
