function retval = parest(model, measure, objective)

%  Function File:	estimates = PAREST(model, measure, objective)
%  
%  Estimates parameters p for differential equation models
%		dx/dt = f(x,t,p)
%		x(0) = x0(p)
%  for given masurements y_i = h(x(t_i)) at time points t_i.
% 
%  The estimates are obtained by solving a weighted least squares problem
% 
%		min Sum ( h(x(t_i)) - y_i )' W ( h(x(t_i)) - y_i )
%		 p   i
%
%  with weight matrix W.
%
%  Input Arguments
%  ===============
%  The three input arguments model, measure, objective are fields and contain:
%
%  1. model
%  --------
%  model.odefcn (required)
%  		Provides the right hand side of the differential equation in the form xdot = f(x, t, parameters).
%
%  model.cvodesfcn (optional)
%		Provides the right hand side of the differential
%		equation in the form expected by CVODES, 
%		[xdot, flag,new_data] = cvodesrhs(t, x, data).  If you provide
%		this function and model.odefcn, then the CVODES RHS
%		function is used and model.odefcn is not called.
%
%  model.dodedp, model.dodedx (recommended)
%  		Partial derivatives of f with respect to the model parameters and the state respectively.
%  		Necessary for exact calculation of gradients of the objective function.
%  
%  model.ic (required)
%  		Initial condition of the system states.
%  
%  model.param (required)
%  		Values of model parameters or initial guesses for the parameters which are estimated.
%  
%  model.par_stateic (optional)
%  		Vector which describes which parameters of model.param are initial conditions of the state.
%  
%  model.stateic_par (optional)
%  		Vector which describes which initial conditions of the state
%  		are parameters (and possibly have to be estimated).
%  		Has same length and ordering as model.par_stateic.
%  
%  model.tplot (optional)
%  		Time points for which estimated states are returned, default: measure.time (cf. below).
%  
%  2. measure
%  ----------
%  measure.states
%  or
%  measure.statefcn, measure.dstatedx (recommended)
%		The user can either provide the measured states as a vector or 
%		a function y = h(x) along with its derivative dh/dx.
%		If neither is provided, parest assumes all states to be measured	
% 
%  measure.time (required)
%		Vector of the measurement times.
%  
%  measure.data (required)
%  		Matrix of data, the rows correspond to the measured states or output functions h respectively and the columns 
%  		correspond to the times provided in measure.time.
%  
%  measure.weight (optional)
%		Matrix W for weighted least squares, default: eye(n).
%  
%  measure.alpha (optional)
% 		Alpha for confidence intervals, default: 0.95
%  
%  3. objective
%  ------------
%  objective.estflag (recommended)
%  		Vector of indices of parameters in model.param that have to be estimated.
%		If estflag is not given, all parameters are estimated.
%		If estflag is an empty vector [], the output values are calculated for
%		the given initial guess of parameters (cf. objective.paric).
%  
%  objective.paric, objective.parlb, objective.parub (recommended)
%  		Vectors of length of estflag with initial guess, lower and 
%		upper bounds for the parameters which are estimated.
%		When no estflag is provided, these vectors have to be of the 
%		same length as model.param.
%		For empty / missing vectors the following defaults are used:
%		Default for objective.paric: corresponding values in model.param
%		Default for objective.parlb: realmin
%		Default for objective.parub: realmax
%		If only scalars are given for objective.parlb, objective.parub, those bounds
%		are used for all parameters.
% 
%  objective.logtr (optional)
%  		Vector of logicals of length of estflag which indicates for 
%		which parameters a logarithmic tranformation should be performed.
%  
%  objective.sensitivityAdjointMethod (optional)
%  		Logical, if true use adjoint method for gradient calculations, if false forward sensitivity analysis is used.
%
%  Return Values
%  =============
%  The return value estimates is a field and contains:
%
%  estimates
%  ---------
%  estimates.parest, estimates.parestlogtr
%  		Estimated parameter values or its logarithm for
%  		those specified in objective.logtr.
%  
%  estimates.obj
%  		Value of the objective function.
%  
%  estimates.grad, estimates.gradlogtr
%  estimates.Hgn, estimates.Hgnlogtr
%  estimates.HFD
%		Gradient and Hessian of objective function with respect to the parameters 
%		or their logarithms for those specified in objective.logtr.
%		The Hessian in Hgn and Hgnlogtr is calculated by using the Gauss-Newton Approximation,
%		whereas HFD is calculated with a two sided finite difference approach.
%
%  estimates.info
%  
%  estimates.mu
%  		If adjoint method is used, matrix of adjoint variable in form mu(:,measure.time).
%  
%  estimates.x
%  estimates.sx
%  		State and sensitivities for the optimal parameters at time tplot or by default the measurement times.
%		x is a matrix with the ith column being the state at the ith time point, i.e. x(:,i) = x(t_i).
%		sx is a three-dimensional structure, with the last argument corresponding to time, i.e. sx(:,:,i) = dx/dp(t_i).
%
%  estimates.measpred
%  		Matrix of predicted measurements with columns corresponding to time points.
%  
%  estimates.resid
%  		Matrix of residuals in the same format as estimates.measpred.
%
%  estimates.meastime
%
%  estimates.var
%  		Estimate of covariance matrix.
%
%  estimates.bbox, estimates.bboxFD
%		Bounding box for confidence intervals for given measure.alpha using the two Hessian matrices.
%
%  See also: -


  global mod_ meas_ obj_

  %% create local copies of these objects to pass to likelihood
  meas_  = measure; obj_ = objective;  mod_ = model;

  %% model.odefcn
  %% model.param 
  %% model.ic  *n-vector*
  %% model.par_stateic *optional*
  %% model.stateic_par *optional*
  %% model.dodedp *new*
  %% model.dodedx *new*
  %% model.tplot *optional*

  %% mod_.sx0 *calculated by parest, not user provided*
  %% obj_.x etc *not user provided, stored for transfer of data between functions*
  %% data.theta *not user provided*

  %% measure.states
  %% measure.statefcn *h(x), x is state vector, h is measurement*
  %% measure.dstatedx *dhdx*
  %% measure.time
  %% measure.data *matrix(state#, time) new*
  %% measure.weight  *optional,default: identity matrix*
  %% measure.alpha *optional: default 0.95*

  %% objective.estflag *optional default:all parameters*
  %% objective.logtr *optional logical*
  %% objective.parlb
  %% objective.parub
  %% objective.paric  *optional default: model.param*
  %% objective.sensitivityAdjointMethod *new logical*

  %% estimates.parest
  %% estimates.parestlogtr *new*
  %% estimates.obj
  %% estimates.grad
  %% estimates.gradlogtr
  %% estimates.Hgn
  %% estimates.Hgnlogtr *new*
  %% estimates.info
  %% estimates.mu *new*
  %% estimates.x
  %% estimates.sx
  %% estimates.measpred
  %% estimates.meastime *new*
  %% estimates.resid
  %% estimates.var *new*
  %% estimates.HFD
  %% estimates.bboxFD

  %% 1. Check model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if(~isfield(mod_,'odefcn'))
	error('parest: model.odefcn missing: no rhs of ode provided');
  end

  if(~isfield(mod_,'param'))
	error('parest: model.param missing');
  end
  
  if(~isfield(mod_,'ic'))
	error('parest: model.ic missing');
  end

  %% if user provides no stateic_par or par_stateic
  if(~isfield(mod_,'stateic_par') | ~isfield(mod_,'par_stateic'))
	mod_.stateic_par = [];
	mod_.par_stateic = [];
  elseif(numel(mod_.stateic_par) ~= numel(mod_.par_stateic) )
	error('parest: model.stateic_par and model.par_stateic must have same length')
  end
 
  if (~isempty(mod_.stateic_par))
  %%  check if entries in model.stateic_par <= number of states
  	if(max(mod_.stateic_par) > length(mod_.ic))
	error('parest: entry in stateic_par exceeds number of states')
    end
  	if(min(mod_.stateic_par) <= 0 )
	error('parest: entry in stateic_par less or equal to zero')
    end
  %%  check if entries in model.par_stateic <= number of parameters
  	if(max(mod_.par_stateic) > length(mod_.param))
	error('parest: entry in par_stateic exceeds number of parameters in model.param')
    end
  	if(min(mod_.par_stateic) <= 0)
	error('parest: entry in par_stateic less or equal to zero')
    end
  end
  %% 2. Check objective %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  %% if user provides no estflag, estimate all parameters
  if(~isfield(obj_,'estflag'))
	obj_.estflag = 1:numel(mod_.param);
  end
  
  %% if estflag is empty, don't estimate any parameters 
  %% but calculate objective function etc
  if(isempty(obj_.estflag))
	obj_.estflag = 1:numel(mod_.param);
	estflagempty = true;
  else
	estflagempty = false;	
  end

  %% number of to estimated parameters
  nparest = numel(obj_.estflag);

  %% if user provides no special initial guess take values out of model.param
  if(~isfield(obj_,'paric') || isempty(obj_.paric) )
	obj_.paric = mod_.param(obj_.estflag);
  end

  %% if user doesn't provide logtr
  if(~isfield(obj_,'logtr') || isempty(obj_.logtr))
	%as default don't use log transform
	obj_.logtr = logical(zeros(nparest,1));
  end

  %% check if sizes in objective are correct
  if (numel(obj_.logtr) ~= nparest)
	error('parest: Number of elements in objective.logtr must be equal to length of objective.estflag') 
  end
  if (~isfield(obj_,'parlb') || isempty(obj_.parlb))
	% if no lower bound is given
	obj_.parlb = realmin*ones(nparest,1);%1e-5*obj_.paric;
	%% error('parest: objective.parlb missing')
  elseif (numel(obj_.parlb) ~= nparest)
	if (isscalar(obj_.parlb))
		obj_.parlb = obj_.parlb*ones(1,nparest);
	else
	error('parest: Number of elements in objective.parlb must be equal to length of objective.estflag')
	end
  end
  if (~isfield(obj_,'parub') || isempty(obj_.parub))
	% if no upper bound is given
	obj_.parub = realmax*ones(nparest,1);%1e5*obj_.paric;
	%% error('parest: objective.parub missing')
  elseif (numel(obj_.parub) ~= nparest)
	if (isscalar(obj_.parub))
		obj_.parub = obj_.parub*ones(1,nparest);
	else
	error('parest: Number of elements in objective.parub must be equal to length of objective.estflag')
	end
  end
  if (numel(obj_.paric) ~= nparest)
	error('parest: Number of elements in objective.paric must be equal to length of objective.estflag')
  end

  %% 3. Check measure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if(~isfield(meas_,'statefcn') && ~isfield(meas_,'states'))
	meas_.states = 1:numel(mod_.ic);
  end

  if(isfield(meas_,'statefcn') && ~isfield(meas_,'dstatedx'))
	error('parest: measure.dstatedx missing');
  end

  if(~isfield(meas_,'data'))
	error('parest: measure.data missing');
  end

  %% if transpose of measurement.data is given
  %% data is supposed to be in format (states,time)
  if (size(meas_.data,2) ~= numel(meas_.time) && size(meas_.data,1) == numel(meas_.time)) 
	meas_.data = (meas_.data)';
  end

  %% number of measurements at each time point, e.g. =2 if x1 and x2 are measured
  nmeas = size(meas_.data, 1);

  %% ensure that measure weight matrix is symmetric
  %% and has appropriate size
  if(isfield(meas_,'weight'))
	if (size(meas_.weight) == nmeas*ones(1,2))
  		meas_.weight = 0.5*(meas_.weight+meas_.weight'); 
	else
		error('parest: size of measurement.weight does not fit to measurement.data')
    end
  else
	meas_.weight = eye(nmeas);
  end

  %% 4. prepare optimization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  %% log transform the specified parameters
	% ensure objective.logtr to be logical
	obj_.logtr = logical(obj_.logtr);
	trind =  obj_.logtr;
	obj_.paric(trind) = log(obj_.paric(trind));
	obj_.parlb(trind) = log(obj_.parlb(trind));
	obj_.parub(trind) = log(obj_.parub(trind));
 
  %% initialize the par sensitivities; note the pars that are ICs
  %% sx0 = 0 for parameters that are not state IC
  %% sx0 = 1 for parameters that are state IC
  mod_.sx0 = zeros(length(mod_.ic), length(obj_.estflag));
  %% which estimated parameters are initial conditions
  [c, iest, istate] = intersect(obj_.estflag, mod_.par_stateic);
  row = mod_.stateic_par(istate);
  %% set mod_.sx0(row, iest) = 1
  %% sensitivity of ICs that are parameters
   if (isvector(row) && ~isempty(row))
	mod_.sx0(sub2ind (size (mod_.sx0), row, iest)) = 1;
   end

  %% 5. call optimizer %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(estflagempty)
	%% if estflag is empty -> no parameters to estimate, no optimization
	theta = obj_.paric;
	info = 'estflag empty, no optimization performed';
else
	%% optimize in order to
	%% find best fit of parameters
	%% solveroption determines if/how gradient is provided for optimization
	if (~isfield(mod_,'dodedx') | ~isfield(mod_,'dodedp') | ~isfield(obj_,'sensitivityAdjointMethod') )
		% user doesn't provide derivatives of ode
		% or user doesn't care about adjoint or forward sensitivity -> forward
		solveroption = 2;
	elseif (obj_.sensitivityAdjointMethod)
		solveroption = 3;
	else
		solveroption = 2;
	endif

	%% minimize negative logarithm of likelihood function
    switch solveroption
    case 1 % solve without provided gradients
	  [theta, phi, info] = fmincon (@likelihood, obj_.paric, ...
        [], [], [], [], obj_.parlb, obj_.parub);
    case 2 % solve using forward sensitivities
    options = optimset('GradObj','on');
    [theta, phi, info] = fmincon ({@likelihood,@gradi}, obj_.paric, ...
        [], [], [], [], obj_.parlb, obj_.parub,[],options);
    case 3	% solve using adjoint method for gradients
    options = optimset('GradObj','on');
    [theta, phi, info] = fmincon ({@likelihoodA,@gradi}, obj_.paric, ...
        [], [], [], [], obj_.parlb, obj_.parub,[],options);
    end

end %if(estflagempty)
 %%[theta, phi, info, lambda] = npsol (obj_.paric, 'likelihood', ...
 %%                           obj_.parlb, obj_.parub);
 %%[theta, phi, info, lambda] = npsol (obj_.paric, ...
 %%          ['likelihood'; 'gradi'], obj_.parlb, obj_.parub);
 %%[theta, phi, info, lambda] = npsol (obj_.paric, ...
 %%          ['likelihoodA'; 'gradi'], obj_.parlb, obj_.parub);
 %%
 %  [theta, phi, info] = fmincon (@likelihood, obj_.paric, ...
 %        [], [], [], [], obj_.parlb, obj_.parub);
 % [theta, phi, info] = sqp(obj_.paric, ...
 %                   {@likelihood; @gradi; @hessi}, [], @hineq);
 % [theta, phi, info] = sqp(obj_.paric, ...
 %                      {@likelihood; @gradi}, [], @hineq);
 % [theta, phi, info] = sqp(obj_.paric, @likelihood, [], @hineq) 

  %% 6. work on return values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  %% (Insurance) call to likelihood with optimal parameters to compute solution,
  %% sensitivities, gradient and Hessian. Not necessary if optimizer's
  %% last call was with optimal values.
	phi = likelihood(theta);
	if (isfield(meas_,'statefcn'))
		retval.measpred = meas_.statefcn(obj_.x);
	else
		retval.measpred = obj_.x(meas_.states,:);
    end
		retval.meastime = meas_.time;

  % if tlot is given, solve ODE for all time points in tplot
  if(isfield(mod_,'tplot'))
  	data.theta = mod_.param;
  	%% set the model parameters; note the log transformation
  	data.theta(obj_.estflag) = theta;
  	trind =  obj_.estflag(obj_.logtr);
  	data.theta(trind) = exp(theta(obj_.logtr));
	  %% solve the model and sensitivites at tplot
		if (~isfield(mod_,'cvodesfcn'))
  		if (~isfield(mod_,'dodedx') | ~isfield(mod_,'dodedp') )
  			[x, sx, status] = cvode_sens('parest:oderhs', [], mod_.ic, ...
			obj_.estflag, data, mod_.sx0, mod_.tplot);
  		else
  			[x, sx, status] = cvode_sens('parest:oderhs', 'parest:sensrhs', mod_.ic, ...
			obj_.estflag, data, mod_.sx0, mod_.tplot);  
  		end
		else
  		if (~isfield(mod_,'dodedx') | ~isfield(mod_,'dodedp') )
  			[x, sx, status] = cvode_sens(mod_.cvodesfcn, [], mod_.ic, ...
			obj_.estflag, data, mod_.sx0, mod_.tplot);
  		else
  			[x, sx, status] = cvode_sens(mod_.cvodesfcn, 'parest:sensrhs', mod_.ic, ...
			obj_.estflag, data, mod_.sx0, mod_.tplot);  
  		end
		end
	% if cvode_sens failed
	if (status ~= 0)
		error('parest: CVode failed with status = %d', status);
    end
  	retval.x = x;
  	retval.sx = sx;
  	%% if no tplot is provided by user return estimates and 
  	%% sensitivities at available measurement time point
  	else
  	retval.x = obj_.x;
  	retval.sx = obj_.sx;
  end

  %% store output for return
	retval.obj = phi;
	retval.info = info;
	retval.parestlogtr = theta;
  %% gradient and Hessian; pull these out of common; computed in likelihood
	retval.resid = obj_.resid;
	retval.grad = obj_.grad;
	retval.gradlogtr = obj_.gradlogtr;
  %% undo log transform the specified parameters
	thetawolog = theta;
	thetawolog(obj_.logtr) = exp(theta(obj_.logtr));
	retval.parest = thetawolog;

  %% if user provides no alpha for confidence intervals
  if(~isfield(meas_,'alpha'))
	meas_.alpha = 0.95;
  end

  if(isfield(obj_,'Hgn'))
	retval.Hgn  = obj_.Hgn;
	retval.Hgnlogtr = obj_.Hgnlogtr;
	  %%compute confidence intervals
	p     = length(theta);
	ndata = length(meas_.time);
	Fstat = finv(meas_.alpha, p, ndata-p);
	Hgn = retval.Hgn;
	if (isscalar(Hgn))
	  invHgn = 1./Hgn;
	else
	  invHgn = inv(Hgn);
    end
        retval.bbox = sqrt(2*p/(ndata-p)*Fstat*phi*diag(invHgn));
  end

  %% adjoint variable mu if computed in likelihoodA
  %% matrix of form mu(:,time)
  if(isfield(obj_,'mu'))
	retval.mu = obj_.mu;
  end

  %% Estimate of Variance Matrix
	residv = retval.resid;
	%% first find and delete bad data
	residv(find(~finite(residv))) = 0;
	%% formula for Variance
	retval.var = residv*residv'/size(residv,2);

  %% approximate Hessian via Finite Difference
	HFD = zeros(nparest,nparest);
	for j =1:nparest
		FDperturbation = 1*sqrt(1e-3)*thetawolog(j);
		thetaFD = thetawolog;
		thetaFD(j) = thetawolog(j)+FDperturbation; %perturb one parameter
        thetaFD2 = thetawolog;
		thetaFD2(j) = thetawolog(j)-FDperturbation; %perturb one parameter
		thetaFD2(obj_.logtr) = log(thetaFD2(obj_.logtr)); %log trans
        likelihood(thetaFD2);
        grad2 = obj_.grad;
        thetaFD(obj_.logtr) = log(thetaFD(obj_.logtr)); %log trans
		likelihood(thetaFD);
		%HFD(:,j) = (obj_.grad - retval.grad)/FDperturbation; one-sided FD
        HFD(:,j) = (obj_.grad - grad2)/(2*FDperturbation); %two sided FD
    end
	HFD = 0.5*(HFD+HFD'); % make HFD symmetric
	retval.HFD = HFD;
	  %%compute confidence intervals with FD
	p     = length(theta);
	ndata = length(meas_.time);
	Fstat = finv(meas_.alpha, p, ndata-p);
	if (isscalar(HFD))
	  invHFD = 1./HFD;
	else
	  invHFD = inv(HFD);
    end
        retval.bboxFD = sqrt(2*p/(ndata-p)*Fstat*retval.obj*diag(invHFD));    
    
%% endfunction %parest.m
%% END OF FUNCTION PAREST.m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% HINEQ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function retval = hineq(theta)
  global obj_
  % provides h_ineq(x) for inequality constraint h_ineq(x) >= 0
  % to ensure that parameters are between lower and upper bound
  % parlb <= theta <= parub
  retval = [theta - obj_.parlb; -theta + obj_.parub];
%% endfunction %hineq
%% GRADI %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function retval = gradi(theta)
  global obj_
  % optimizer should have just called the function likelihood with the same
  % theta value, and likelihood has computed obj_.gradlogtr.
  % So pull the gradient out of the global variables and return
  retval = obj_.gradlogtr';
%% endfunction %gradi
%% ODERHS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xdot, flag, new_data] = oderhs(t, x, data)
  global mod_
  % Right hand side of differential equation
  % dx/dt = f(x,t,theta)
  [xdot] = mod_.odefcn(x, t, data.theta);
  flag = 0;
  new_data = [];
%% endfunction %oderhs
%% SENSRHS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xsd, flag, new_data] = sensrhs(t, x, xdot, xs, data)
  global mod_ obj_
  % Right hand side of forward sensitivity equation
  % sx = dx/dp
  % dsx / dt = df/dx * sx + df/dp
  dfdp = mod_.dodedp(x, t, data.theta);
  dfdp = dfdp(:,obj_.estflag);
  xsd = mod_.dodedx(x, t, data.theta)*xs + dfdp;
  flag = 0;
  new_data = [];
%% endfunction %sensrhs

%% CVODE_SENS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [x, sx, status] = cvode_sens(f, fp, x0, estflag, data, sx0, tmeas)
% Call the cvode integrator and calculate states, x, and 
% sensitivities, sx, at the requested times.
% f is rhs of ode, fp is rhs of sensitivity ode
% status = 0 -> no errors

  %% Set options for integrator.
options = CVodeSetOptions ('RelTol', sqrt (eps), ...
			   'AbsTol', sqrt (eps));
t0 = 0;

  %% Allocate storage and set initial conditions for integrator.
CVodeMalloc (f, t0, x0, options, data);

  %% Number of paramters
np = numel(estflag);

  %% Set options for forward sensitivity problem.
if (isempty(fp))
  fsa_options = CVodeSetFSAOptions('SensErrControl', 'on',...
                                   'ParamField', 'theta',...
                                   'ParamList', estflag,...
                                   'ParamScales', ones(1, np));
else
  fsa_options = CVodeSetFSAOptions ('SensErrControl', 'on',...
                                    'SensRhsFn', fp,...
                                    'ParamField', 'theta',...
                                    'ParamList', estflag,...
                                    'ParamScales', ones(1, np));
end

  %% Allocate storage for forward sensitivity problem.
CVodeSensMalloc (np, 'Simultaneous', sx0, fsa_options);

  %% Integrate and save output.
nx = numel (x0);
nt = numel (tmeas);

x = zeros (nx,nt);
sx = zeros (nx, np,nt);

x(:,1) = x0;
sx(:,:,1) = sx0;

iout = 1;
measic = 0;
if (tmeas(1) == t0)  measic = 1; end
tout = tmeas(1+measic);
nsteps = nt - measic + 1;

while (true)
  [status, tau, x_step, sx_step] = CVode(tout, 'Normal');
  if (status == 0)
    iout = iout + 1;
    x(:,iout) = x_step;
    sx(:,:,iout) = sx_step;
  elseif (status < 0)
	warning ('parest: CVode failed with status = %d', status);
	break;
  end
  if (iout == nsteps)
    break;
  end
  tout = tmeas(iout + measic);
end

  %% Free integrator storage.
  CVodeFree ();
%% endfunction cvodes_sens %%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% LIKELIHOOD %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function phi = likelihood(theta)
% Calculates negative logarithm of likelihood function,
% this function is minimized by parest.m.
% Gradients and sensitivities are calculated by forward calculations
% using cvode_sens.
  global mod_ meas_ obj_
  %% data.theta contains all parameters, not only those which are estimated!
  data.theta = mod_.param;
  %% set the model parameters; note the log transformation
  data.theta(obj_.estflag) = theta;
  trind =  obj_.estflag(obj_.logtr);
  data.theta(trind) = exp(theta(obj_.logtr));

  %% set the state ICs that are parameters
  mod_.ic(mod_.stateic_par)= data.theta(mod_.par_stateic);

  %% solve the model and sensitivites at the measurement times;
  %% predict measurements
		if (~isfield(mod_,'cvodesfcn'))
  		if (~isfield(mod_,'dodedx') | ~isfield(mod_,'dodedp') )
  			[x, sx, status] = cvode_sens('parest:oderhs', [], mod_.ic, ...
			obj_.estflag, data, mod_.sx0,  meas_.time);
  		else
  			[x, sx, status] = cvode_sens('parest:oderhs', 'parest:sensrhs', mod_.ic, ...
			obj_.estflag, data, mod_.sx0,  meas_.time);  
  		end
		else
  		if (~isfield(mod_,'dodedx') | ~isfield(mod_,'dodedp') )
  			[x, sx, status] = cvode_sens(mod_.cvodesfcn, [], mod_.ic, ...
			obj_.estflag, data, mod_.sx0,  meas_.time);
  		else
  			[x, sx, status] = cvode_sens(mod_.cvodesfcn, 'parest:sensrhs', mod_.ic, ...
			obj_.estflag, data, mod_.sx0,  meas_.time);  
  		end
		end
  % if cvode_sens failed
	if (status ~= 0)
		phi = realmax;
		return;
    end

  nmeas = size(meas_.data, 1);
  ntimes = size(meas_.data, 2);

  resid = zeros(nmeas,ntimes);
  nx = numel (mod_.ic);
  npar = numel(theta);

    %% is a loop best here?
    phi = 0;
    grad = zeros(npar, 1);
    Hgn  = zeros(npar, npar);
    for i = 1: ntimes
	if (isfield(meas_,'statefcn'))
		% if user provides a measurement state function
		resid(:,i) = meas_.data(:,i) - meas_.statefcn(x(:,i));
		% Jacobian of h at time point i if statefcn provided by user
		sqdhdx = meas_.dstatedx(x(:,i));
	else
		% if user provides only vector of measured states
		resid(:,i) = meas_.data(:,i) - x(meas_.states,i);
		IdentityMatrix = eye(nx);
		% Jacobian at of h
		sqdhdx = IdentityMatrix(meas_.states,:);
    end

	sqsx = sx(:,:,i);
	r = resid(:,i);

	%% look for bad data (inf, NaN,...) and do not use it for calculation
		badData = find(~finite(r));
		r(badData) = 0;
		sqsx(badData,:) = 0;

	tmp = sqdhdx*sqsx;
	phi = phi + r'*meas_.weight*r;
	grad = grad + tmp'*meas_.weight*r;
	Hgn = Hgn + tmp'*meas_.weight*tmp;
    end %i = 1: ntimes

  obj_.resid = resid;
  %% gradient with respect to untransformed parameters
  grad = -2*grad';
  obj_.grad = grad;
  %% gradient with respect to log transformed parameters
  %% this is the optimizer's gradient (see gradi function)
  scale = ones(npar,1);
  scale(obj_.logtr) = data.theta(trind);
  obj_.gradlogtr = obj_.grad*diag(scale);
  %% Gauss Newtown approximation of Hessian with untransformed parameters
  obj_.Hgn =  2*Hgn;
  obj_.Hgnlogtr =  2*diag(scale)*Hgn*diag(scale);
  obj_.x = x;
  obj_.sx = sx;
%% endfunction %sensrhs
%% END OF FUNCTION LIKELIHOOD.m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% LIKELIHOODA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function phi = likelihoodA(theta)
% Calculates negative logarithm of likelihood function,
% this function is minimized by parest.m.
% Gradients are calculated by adjoint method.
theta
  global mod_ meas_ obj_ data
  %% data.theta contains all parameters, not only those which are estimated!
  data.theta = mod_.param;
  %% set the model parameters; note the log transformation
  data.theta(obj_.estflag) = theta;
  trind =  obj_.estflag(obj_.logtr);
  data.theta(trind) = exp(theta(obj_.logtr));

  %% set the state ICs that are parameters
  mod_.ic(mod_.stateic_par) = data.theta(mod_.par_stateic);

  %% Set options for integrator.
  options = CVodeSetOptions ('RelTol', sqrt (eps), 'AbsTol', sqrt (eps));

  %% Allocate storage and set initial conditions for integrator.
  if (~isfield(mod_,'cvodesfcn'))
  CVodeMalloc (@oderhs, 0, mod_.ic, options, data);
  else
  CVodeMalloc (mod_.cvodesfcn, 0, mod_.ic, options, data);
  end
  tmeas = meas_.time;
  t0 = 0;
  t1 = tmeas(end);
  nmeas = size(meas_.data, 1);
  ntimes = size(meas_.data, 2);
  nx = numel (mod_.ic);
  npar = numel(obj_.estflag);

  x = zeros (nx,ntimes);
  mu = zeros (nx,ntimes);

  x(:,1) = mod_.ic;

  %% Integrate forward, saving intermediate points for calculation of
  %% the objective function.
  iout = 1;
  measic = 0;
  if (tmeas(1) == 0)  measic = 1; end
  tout = tmeas(1+measic);
  nsteps = ntimes - measic + 1;

  while (true)
  [status, t, x_step] = CVode (tout, 'Normal');
  if (status == 0)
    iout = iout + 1;
    x(:,iout) = x_step;
  else
		warning ('parest: CVode failed with status = %d', status);
		break;
  end
  if (iout == nsteps)
    break;
  end
  tout = tmeas(iout + measic);
  end

  if(status < 0)
	CVodeFree ();
	phi = realmax;
	return;
  end

  %% Allocate memory for checkpointing of forward solution for
  %% backward integration of adjoint problem.
  CVadjMalloc (150, 'Hermite');

  %%compute residual and objective function for these param values
  %%compute gradient, here NO Hessian approximation; have to store in common
  if (isfield(meas_,'statefcn'))
		% if user provides a measurement state function
		statefcnExists = true;
		resid = zeros(nmeas,ntimes);
		for t_ind = 1:ntimes	%loop over all time points
			resid(:,t_ind) = meas_.data(:,t_ind) - meas_.statefcn(x(:,t_ind));
        end
	else
		% if user provides only vector of measured states
		statefcnExists = false;
		resid = meas_.data - x(meas_.states,:);
		IdentityMatrix = eye(nx);
		dhdx = IdentityMatrix(meas_.states,:);
  end
  obj_.resid = resid;
  %% find and delete bad data
  resid(find(~finite(resid))) = 0;

  phi = trace(resid'*meas_.weight*resid);

  %% Free storage for the integration.
  %CVodeFree ();

  %% Set up reverse integration.  Must be done after the forward
  %% integration is complete.

  %% Initial condition for the backward quadrature.
  w = zeros (1, npar);

  %% Set options for the backward integration of the adjoint equation and
  %% quadrature.
  optionsb = CVodeSetOptions('Quadratures', 'on', 'QuadRhsFn', @sensrhsA, 'QuadInitcond', w, 'QuadErrControl', 'on', 'QuadRelTol', sqrt (eps),'QuadAbsTol', sqrt (eps),'RelTol', sqrt (eps),'AbsTol', sqrt (eps));

  %% Initial condition for the backward integration of the adjoint
  %% equations
	if (statefcnExists)
		% Jacobian at last time point if statefcn provided by user
		sqdhdx = meas_.dstatedx(x(:,end));
		%% find and delete bad data
		sqdhdx(find(~finite(meas_.data(:,end))),:) = 0;
	else
		% Jacobian at last time point if only measurement provided by user
		sqdhdx = dhdx;
		%% find and delete bad data
		sqdhdx(find(~finite(meas_.data(:,end))),:) = 0;
    end
  mu_init = -2*( resid(:,end)'*meas_.weight*sqdhdx )';
  mu(:,end) = mu_init;
  %% Allocate storage and set initial conditions for integrator.
  CVodeMallocB (@adjrhs, t1, mu_init, optionsb);

  %% Integrate backward, accumulating in w the gradient of phi, which is
  %% the least-squares objective \sum resid*meas_.weight*resid.
  for(i = ntimes-1:-1:1)
	tinit = tmeas(i+1);
	tout = tmeas(i);
	CVodeReInitB (@adjrhs, tinit, mu_init, optionsb);
    while (true)
	[status, t_step, mu_step, w_step] = CVodeB(tout, 'Normal');
    	if (status < 0)
		warning ('parest: CVodeB failed with status = %d', status);
		break;
        end
	%% We are integrating backward, and we are storing the results in
	%% reverse order too.
    	if (status == 0 || (status == 1 && t_step == 0))
	if (statefcnExists)
		% Jacobian at time point i if statefcn provided by user
		sqdhdx = meas_.dstatedx(x(:,i));
		%% find and delete bad data
		sqdhdx(find(~finite(meas_.data(:,i))),:) = 0;
	else
		% Jacobian at time point i if only measurement provided by user
		sqdhdx = dhdx;
		%% find and delete bad data
		sqdhdx(find(~finite(meas_.data(:,i))),:) = 0;
    end
      	mu_init = mu_step - 2*(resid(:,i)'*meas_.weight*sqdhdx )';
      	mu(:,i) = mu_init;
      	w = w + w_step';
      	break;
        end
    end
  end

if( tmeas(1) > 0) %% integrate till t = 0
  CVodeReInitB (@adjrhs, tmeas(1), mu_init, optionsb);
  [status, t_s, mu_0, w_0] = CVodeB (t0, 'Normal');
    	if (status ~= 0)
		warning ('parest: CVodeB failed with status = %d', status);
        else
        w = w + w_step';
        end
else
  mu_0 = mu_init;
end

% Free storage for the integration.
CVodeFree ();
%CVadjFree ();

    %% gradient with respect to untransformed parameters
    %% Eqn (3.19) of CVodeS user guide
    %% sx0 = 0 for parameters that are not state IC
    %% sx0 = 1 for parameters that are state IC
    grad = w + mu_0'*mod_.sx0;
    obj_.grad = grad;
    %% gradient with respect to log transformed parameters
    %% this is the optimizer's gradient (see gradi function)
    scale = ones(npar,1);
    scale(trind) = data.theta(trind);
    obj_.gradlogtr = obj_.grad*diag(scale);
    %% values of x at tmeas
    obj_.x = x;
    obj_.mu = mu;

%% END OF FUNCTION LIKELIHOODA.m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [qbd, flag, new_data] = sensrhsA (t, x, mu, data)
  global mod_ obj_
  % Backward quadrature function use to compute gradient of objective
  % function.  In this problem, it computes Equation 3.19 from the CVodes
  % user guide:
  %   dg/dp(t1) = mu(t0)s(t0) + g_p(t1) + \int_t0^t1 mu'*f_p
  % 
  % compute the integral by integrating
  %    dw/dt = - mu' f_p
  %    w(t1) = 0
  % 
  %  backward over the time horizon.
  dfdp = mod_.dodedp(x, t, data.theta);
  dfdp = dfdp(:,obj_.estflag);
  qbd = -(mu'*dfdp)';
  flag = 0;
  new_data = [];
%% endfunction % sensrhsA

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [mudot, flag, new_data] = adjrhs (t, x, mu, data)
  global mod_
  % RHS of adjoint system.  In this problem, it computes Equation 3.20
  % from the CVodes user guide:
  %    mudot = - f_x'*mu
  %    mu(t1) = g_x' at t=t1
  mudot = -mod_.dodedx(x, t, data.theta)'*mu;
  flag = 0;
  new_data = [];
%% endfunction %adjrhs