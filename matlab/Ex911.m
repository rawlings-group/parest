function a = Ex911()
startup_STB
%% Exercise 9.11
close all
clear all
randn('seed',0);
disp('*********** Exercise 9.11 *************')

global datatheta

% simulation time
tfinal = 5;
nts    = 100;
tout = linspace(0,tfinal,nts)';

% true model parameters
kac   = 0.1;
nac   = 2;
mac   = 1;

% initial conditions
ca0 = [2; 5];
cb0 = [4; 3];
nsets = length(ca0);

%% DATASET 1 or 2
for i = 1: nsets
% set initial condition    
ca0ac = ca0(i);
cb0ac = cb0(i);
%ca0ac = [5];
%cb0ac = [3];
%ca0ac = [2];
%cb0ac = [4];
% set parameter vector
param = [ca0ac; cb0ac; kac; nac; mac];
datatheta = param;
model.param   = param;

% rhs of ode and its derivatives
model.odefcn  = @ode;
model.dodedx  = @dfdx; %
model.dodedp  = @dfdtheta; %
  
% solve model with true parameters to create measurement data
  x0  = [ca0ac; cb0ac];
  x = ode15s(@react2massbal,tout,x0);
  meas = deval(x,tout);
% add measurement noise
  measvar = 1e-2;
  measstddev = sqrt(measvar);
  noise = measstddev*randn(length(tout),2);
  ymeas = meas + noise';
% store data for calculations with both datasets
  ymeas2(2*i-1:2*i,:) = ymeas;
% which  model parameters are initial conditions of which model states
model.par_stateic = [1 2];
model.stateic_par = [1 2];

% set initial conditions that are not model parameters;
model.ic  = [ca0ac;cb0ac];

% measurement data + options
measure.data = ymeas; 
measure.time = tout;
measure.weight = eye(2);
measure.alpha = 0.95;
measure.states = [1 2];

% objective
% which parameters are estimated
objective.estflag = [1 2 3 4 5];
objective.paric   = 1.1*param(objective.estflag);
objective.parlb   = objective.paric*exp(-2.5); %lower bound
objective.parub   = objective.paric*exp(1.5);  % upper bound
objective.logtr   = logical(zeros(numel(objective.estflag),1)); % use log transformation of parameters?
%objective.sensitivityAdjointMethod = true; %optional

   
% estimate the parameters
estimates = parest(model,measure, objective);
disp('---Dataset------')
disp(i)
Var = estimates.var
phi = estimates.obj
par = [estimates.parest]
bb =[estimates.bbox]
bbFD =[estimates.bboxFD]
Hgn = [estimates.Hgn]
%HgnFD = [estimates.HgnFD]

end

%% BOTH DATASETS
% here both data sets are considered as a 4-dim system [ca1; cb1; ca2; cb2]
% set parameters
param = [ca0(1); cb0(1); ca0(2); cb0(2); kac; nac; mac];
datatheta = param;
model.param   = param;

% rhs of ode and its derivatives
model.odefcn  = @ode2;
model.dodedx  = @dfdx2; %
model.dodedp  = @dfdtheta2; %
% which  model parameters are initial conditions of which model states
model.par_stateic = [1 2 3 4];
model.stateic_par = [1 2 3 4];

% set initial conditions that are not model parameters;
model.ic  = [ca0(1); cb0(1); ca0(2); cb0(2)];

% measurement data + options
measure.data = ymeas2;
measure.time = tout;
measure.weight = eye(4);
measure.alpha = 0.95;
measure.states = [1 2 3 4];

% estimate all parameters
% 1-4 are IC's of states
% 5-7 are parameters k,n,m
objective.estflag = [1 2 3 4 5 6 7];
objective.paric   = 1.1*param(objective.estflag);
objective.parlb   = objective.paric*exp(-2.5); %lower bound
objective.parub   = objective.paric*exp(1.5);  % upper bound
objective.logtr   = logical(zeros(numel(objective.estflag),1)); % use log transformation of parameters?
%objective.sensitivityAdjointMethod = true; %optional

% estimate the parameters
estimates = parest(model,measure, objective);
disp('---BOTH Datasets---')
Var = estimates.var
phi = estimates.obj
par = [estimates.parest]
Hgn = [estimates.Hgn]
bb =[estimates.bbox]
%HFD = [estimates.HFD]
%bbFD =[estimates.bboxFD]

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% user defined functions
  function [xdot] =ode(x, t, theta)
    ca      = x(1);
    cb      = x(2);
    k       = theta(3);
    n       = theta(4);
    m       = theta(5);
    xdot = zeros(2,1);
    if ( (ca <= 0.0) || (cb <= 0.0) )
      xdot(1) = 0;
      xdot(2) = 0;
    else
      xdot(1) = -k*ca^n*cb^m;
      xdot(2) = -k*ca^n*cb^m;
    end
    
  function [xdot] =dfdx(x, t, theta)
    ca      = x(1);
    cb      = x(2);
    k       = theta(3);
    n       = theta(4);
    m       = theta(5);
    xdot = zeros(2,2);
    if ( (ca <= 0.0) || (cb <= 0.0) )
      xdot = zeros(2,2);
    else
      xdot(1,:) = [-k*ca^(n-1)*cb^m,-k*ca^n*cb^(m-1)];
      xdot(2,:) =   [-k*ca^(n-1)*cb^m,-k*ca^n*cb^(m-1)];
    end
    
    function [xdot] =dfdtheta(x, t, theta)
    ca      = x(1);
    cb      = x(2);
    k       = theta(3);
    n       = theta(4);
    m       = theta(5);
    xdot = zeros(2,5);
    if ( (ca <= 0.0) || (cb <= 0.0) )
      xdot = zeros(2,2);
    else
      xdot(1,:) = [0,0,-ca^n*cb^m,-k*log(ca)*ca^n*cb^m,-k*log(cb)*ca^n*cb^m];
      xdot(2,:) =   [0,0,-ca^n*cb^m,-k*log(ca)*ca^n*cb^m,-k*log(cb)*ca^n*cb^m];
    end

  function xdot = react2massbal(t,x)
    global datatheta
    ca      = x(1);
    cb      = x(2);
    k       = datatheta(3);
    n       = datatheta(4);
    m       = datatheta(5);
    xdot = zeros(2,1);
    if ( (ca <= 0.0) || (cb <= 0.0) )
      xdot(1) = 0;
      xdot(2) = 0;
    else
      xdot(1) = -k*ca^n*cb^m;
      xdot(2) = -k*ca^n*cb^m;
    end
    
  function xdot = react2massbal2(t,x)
    global datatheta
    ca      = x(1);
    cb      = x(2);
    ca2      = x(3);
    cb2      = x(4);
    k       = datatheta(5);
    n       = datatheta(6);
    m       = datatheta(7);
    xdot = zeros(4,1);
    if ( (ca <= 0.0) || (cb <= 0.0) )
      xdot = zeros(4,1);
    else
      xdot(1) = -k*ca^n*cb^m;
      xdot(2) = -k*ca^n*cb^m;
      xdot(3) = -k*ca2^n*cb2^m;
      xdot(4) = -k*ca2^n*cb2^m;
    end
    
    function [xdot] =ode2(x, t, theta)
    ca      = x(1);
    cb      = x(2);
    ca2      = x(3);
    cb2      = x(4);
    k       = theta(5);
    n       = theta(6);
    m       = theta(7);
    xdot = zeros(4,1);
    if ( (ca <= 0.0) || (cb <= 0.0) )
      xdot =zeros(4,1) ;
    else
      xdot(1) = -k*ca^n*cb^m;
      xdot(2) = -k*ca^n*cb^m;
      xdot(3) = -k*ca2^n*cb2^m;
       xdot(4) = -k*ca2^n*cb2^m;
    end
    
  function [xdot] =dfdx2(x, t, theta)
    ca      = x(1);
    cb      = x(2);
    ca2      = x(3);
    cb2      = x(4);
    k       = theta(5);
    n       = theta(6);
    m       = theta(7);
    xdot = zeros(4,4);
    if ( (ca <= 0.0) || (cb <= 0.0) )
      xdot = zeros(4,4);
    else
        xd = zeros(2,2);
        xd2 = zeros(2,2);
      xd(1,:) = [-k*ca^(n-1)*cb^m,-k*ca^n*cb^(m-1)];
      xd(2,:) =   [-k*ca^(n-1)*cb^m,-k*ca^n*cb^(m-1)];
      xd2(1,:) = [-k*ca2^(n-1)*cb2^m,-k*ca2^n*cb2^(m-1)];
      xd2(2,:) =   [-k*ca2^(n-1)*cb2^m,-k*ca2^n*cb2^(m-1)];
      xdot = [xd, zeros(2,2); zeros(2,2),xd2];
    end
    
    function [xdot] =dfdtheta2(x, t, theta)
    ca      = x(1);
    cb      = x(2);
    ca2      = x(3);
    cb2      = x(4);
    k       = theta(5);
    n       = theta(6);
    m       = theta(7);
    xdot = zeros(4,7);
    if ( (ca <= 0.0) || (cb <= 0.0) )
      xdot = zeros(4,7);
    else
      xdot(1,:) = [0,0,0,0,-ca^n*cb^m,-k*log(ca)*ca^n*cb^m,-k*log(cb)*ca^n*cb^m];
      xdot(2,:) =   [0,0,0,0,-ca^n*cb^m,-k*log(ca)*ca^n*cb^m,-k*log(cb)*ca^n*cb^m];
      xdot(3,:) = [0,0,0,0,-ca2^n*cb2^m,-k*log(ca2)*ca2^n*cb2^m,-k*log(cb2)*ca2^n*cb2^m];
      xdot(4,:) =   [0,0,0,0,-ca2^n*cb2^m,-k*log(ca2)*ca2^n*cb2^m,-k*log(cb2)*ca2^n*cb2^m];
    end
    
    
    