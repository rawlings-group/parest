function a = Ex95()
startup_STB
% Exercise 9.5
close all
clear all
disp('**********Exercise 9.5*********')
global data model

% time parameters
tfinal = 40;
nplot = 100;
model.tplot   = linspace(0,tfinal,nplot);

% rhs of ode and its derivatives
model.odefcn  = @ode;
model.dodedx  = @dfdx; %
model.dodedp  = @dfdtheta; %

% parameters of model
param = [0.04, 0.8, 1.2]';

model.param   = param;

% which  model parameters are initial conditions of which model states
model.par_stateic = [2];
model.stateic_par = [1];

% set initial conditions
model.ic  = [1];
% which states are measured
measure.states = [1];
% measured data / time points
measure.time = [0;4;8;12;16;20;24;28;32;36;40];
dataset(1,:) = [0.83, 0.696, 0.591, 0.507, 0.44, 0.384, 0.339, 0.301, 0.268, 0.241, 0.217];
dataset(2,:) = [0.833, 0.683, 0.593, 0.515, 0.446, 0.38, 0.324, 0.303, 0.256, 0.225, 0.205];
dataset(3,:) = [0.862, 0.699, 0.561, 0.511, 0.412, 0.407, 0.346, 0.276, 0.322, 0.254, 0.215];

% options for optimization
measure.weight = 1;
measure.alpha = 0.95;

% which parameters are estimated
objective.estflag = [1 2 3];%XXX
% initial guess for parameters
objective.paric   = 1.1*param(objective.estflag);
objective.parlb   = objective.paric*exp(-1.5); %lower bound
objective.parub   = objective.paric*exp(1.5);  % upper bound
% use log transform
objective.logtr   = logical(zeros(numel(objective.estflag),1)); % use log transformation of parameters?
%objective.sensitivityAdjointMethod = true; %optional

%% DATASETS 1 to 3
for i = 1:3
    measure.data = dataset(i,:);
    % estimate the parameters
estimates{i} = parest(model,measure, objective);
disp('-----Dataset') 
disp(i)
Var = estimates{i}.var
phi = estimates{i}.obj
par = [estimates{i}.parest]
Hgn = [estimates{i}.Hgn]
bb =[estimates{i}.bbox]
%HFD = [estimates{i}.HFD]
%bbFD =[estimates{i}.bboxFD]

end

%% END OF FUNCTTION

%% USER DEFINED FUNCTIONS
function [xdot] =ode(x, t, theta) % rhs of ode = f(x,t,p)
  k = theta(1);
  cA0 = theta(2);
  n = theta(3);
  xdot= -k*x^n;
 
function xdot = dfdtheta(x, t, theta) % df/dp
   k = theta(1);
  cA0 = theta(2);
  n = theta(3);
  xdot= [-x^n, 0, -k*log(x)*x^n];

function xdot = dfdx(x, t, theta) % df/dx
   k = theta(1);
  cA0 = theta(2);
  n = theta(3);
  xdot= [-k*n*x^(n-1)];