function [mudot, flag, new_data] = adjrhs (t, x, mu, data)
% RHS of adjoint system.  In this problem, it computes Equation 3.20
% from the CVodes user guide:
%    mudot = - f_x'*mu
%    mu(t1) = g_x' at t=t1
global mod_
  mudot = -mod_.dodedx(x, t, data.theta)'*mu;
  flag = 0;
  new_data = [];
testadjrhs = 0
endfunction
