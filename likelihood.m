function phi = likelihood(theta)
%% Calculates negative logarithm of likelihood function,
%% this function is minimized by parest.m.
%% Gradients and sensitivities are calculated by forward calculations
%% using cvode_sens.
0
  global mod_ meas_ obj_
  %% data.theta contains all parameters, not only those which are estimated!
  data.theta = mod_.param;
  %% set the model parameters; note the log transformation
  data.theta(obj_.estflag) = theta;
  trind =  obj_.estflag(obj_.logtr);
  data.theta(trind) = exp(theta(obj_.logtr));

  %% set the state ICs that are parameters
  mod_.ic(mod_.stateic_par)= data.theta(mod_.par_stateic);

  %% solve the model and sensitivites at the measurement times;
  %% predict measurements
		if (~isfield(mod_,'cvodesfcn'))
  		if (~isfield(mod_,'dodedx') || ~isfield(mod_,'dodedp') )
  			[x, sx, status] = cvode_sens(@oderhs, [], mod_.ic, ...
			obj_.estflag, data, mod_.sx0,  meas_.time);
  		else
  			[x, sx, status] = cvode_sens(@oderhs, @sensrhs, mod_.ic, ...
			obj_.estflag, data, mod_.sx0,  meas_.time);  
  		endif
		else
  		if (~isfield(mod_,'dodedx') || ~isfield(mod_,'dodedp') )
  			[x, sx, status] = cvode_sens(mod_.cvodesfcn, [], mod_.ic, ...
			obj_.estflag, data, mod_.sx0,  meas_.time);
  		else
  			[x, sx, status] = cvode_sens(mod_.cvodesfcn, @sensrhs, mod_.ic, ...
			obj_.estflag, data, mod_.sx0,  meas_.time);  
  		endif
		endif
  % if cvode_sens failed
	if (status ~= 0)
		phi = realmax;
		return;
	endif

  nmeas = size(meas_.data, 1);
  ntimes = size(meas_.data, 2);

  resid = zeros(nmeas,ntimes);
  nx = numel (mod_.ic);
  npar = numel(theta);

    %% is a loop best here?
    phi = 0;
    grad = zeros(npar, 1);
    Hgn  = zeros(npar, npar);
    for i = 1:ntimes
	if (isfield(meas_,'statefcn'))
		% if user provides a measurement state function
		resid(:,i) = meas_.data(:,i) - meas_.statefcn(x(:,i));
		% Jacobian of h at time point i if statefcn provided by user
		sqdhdx = meas_.dstatedx(x(:,i));
	else
		% if user provides only vector of measured states
		resid(:,i) = meas_.data(:,i) - x(meas_.states,i);
		IdentityMatrix = eye(nx);
		% Jacobian at of h
		sqdhdx = IdentityMatrix(meas_.states,:);
	endif

	sqsx = sx(:,:,i);
	r = resid(:,i);

	%% look for bad data (inf, NaN,...) and do not use it for calculation
		badData = find(~finite(r));
		r(badData) = 0;
		sqsx(badData,:) = 0;

	tmp = sqdhdx*sqsx;
	phi = phi + r'*meas_.weight*r;
	grad = grad + tmp'*meas_.weight*r;
	Hgn = Hgn + tmp'*meas_.weight*tmp;
    endfor %i = 1: ntimes

  obj_.resid = resid;
  %% gradient with respect to untransformed parameters
  grad = -2*grad';
  obj_.grad = grad;
  %% gradient with respect to log transformed parameters
  %% this is the optimizer's gradient (see gradi function)
  scale = ones(npar,1);
  scale(obj_.logtr) = data.theta(trind);
  obj_.gradlogtr = obj_.grad*diag(scale);
  %% Gauss Newtown approximation of Hessian with untransformed parameters
  obj_.Hgn =  2*Hgn;
  obj_.Hgnlogtr =  2*diag(scale)*Hgn*diag(scale);
  obj_.x = x;
  obj_.sx = sx;

%% check on where the optimizer is looking
%theta
%phi
endfunction
% END OF FUNCTION LIKELIHOOD.m %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


