function retval = gradi(theta)
  global obj_
  %% optimizer should have just called the function likelihood with the same
  %% theta value, and likelihood has computed obj_.gradlogtr.
  %% So pull the gradient out of the global variables and return
  retval = obj_.gradlogtr';
testgradi = 0
endfunction
