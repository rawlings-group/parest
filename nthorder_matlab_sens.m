%% Copyright (C) 2007, James B. Rawlings, Marcus Reble, and John W. Eaton 
%%
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation; either version 2, or (at
%% your option) any later version.
%%
%% This program is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program; see the file COPYING.  If not, write to
%% the Free Software Foundation, 59 Temple Place - Suite 330, Boston,
%% MA 02111-1307, USA.

%%
%% jbr,  1/16/2007
%%
%% develop a prototype parameter estimation  interface
%%

clear all
close all

% ensure sundials/cvode is in the path
addpath (genpath("~/src/sundialsTB"));

% remove this global after fixing massbal_fixme
global model 

% for debugging likelihood; remove later
global obj_

kac   = 2.5;
ca0ac = 1;
nac   = 1.5;
thetaac = [kac; ca0ac; nac];
tfinal = 5;
nplot = 120;


function xdot = massbal_ode(x, t, theta)
  ca      = x(1);
  k       = theta(1);
  n       = theta(3);
  if (ca <= 0.0)
    xdot = [0;0];
  else
    xdot = [-k*ca^n; -k*x(2)];
  endif
  flag = 0;
  new_data = [];
endfunction

function xsd = massbal_deriv(x, t, theta) %derivative wrt theta
  ca      = x(1);
  k       = theta(1);
  n       = theta(3);
  if (ca <= 0.0)
    xsd = zeros(2, 3);
  else
%    xsd = [-ca^n, 0, -k*n*log(ca)] - k*n*ca^(n-1)*xs;
     xsd = [-ca^n, 0, -k*log(ca)*ca^(n); -x(2), 0,0];
  endif
endfunction

function deriv = massbal_dx(x, t, theta) %derivative wrt x
  ca      = x(1);
  k       = theta(1);
  n       = theta(3);
  if (ca <= 0.0)
    deriv = zeros(2,2);
  else
    deriv = [-k*n*ca^(n-1),0; 0,-k];
  endif
endfunction


  function retval = massbal_fixme(t, x)
    global model
    data.theta = model.param;
    retval = massbal_ode(x, t, data.theta);
  endfunction

function retval = h(x)
  %% something simple, measure the state
  retval = x;
endfunction

function retval = dh(x)
  %% dh(x)/dx;
%    nstate = size(x, 2);
%    ntime  = size(x, 1);
%    E = eye(nstate);
%    a = reshape(E, [1, size(E)]);
%    retval = repmat(a, [ntime, 1, 1]);
	retval = eye(numel(x));
endfunction

model.odefcn  = @massbal_ode;
%model.dodedp = [];
%model.dodedx  = [];
model.dodedx  = @massbal_dx; 
model.dodedp  = @massbal_deriv;
model.tplot   = linspace(0,2*tfinal,nplot);
model.param   = thetaac;
%% which  model parameters are initial conditions of which model states
model.par_stateic = [2];
model.stateic_par = [1];
model.ic(model.stateic_par)= model.param(model.par_stateic);
model.ic(2) = 2;
model.ic = model.ic';

measure.statefcn = @h;
measure.dstatedx = @dh;
measure.states = [1 2];
%measure.dstatedx = [];
measure.time = linspace(0, 5, 100);

objective.estflag = [1,2,3];%XXXXXX
%model.par_stateic = [];
%model.stateic_par = [];

nest = length(objective.estflag);
% objective.paric   = thetaac(objective.estflag);
objective.paric   = [1;2;3.5];
objective.paric   = objective.paric(objective.estflag);
objective.logtr   = [];
%objective.logtr   = logical(zeros(nest,1)); % use log transformation of parameters?
%objective.logtr   = logical(ones(nest,1)); % use log transformation of parameters?
objective.parlb   = [1e-4; 1; 0.2];
objective.parlb   = objective.parlb(objective.estflag);
objective.parub   = [10; 5; 5];
objective.parub   = objective.parub(objective.estflag);

%%create the measurements by solving the model and adding noise
[tsolver,caac] = ode15s(@massbal_fixme, measure.time, model.ic);
randn('seed',0);
measure.data = (h(caac) + 0.05*randn(numel(measure.time), 2))';
measure.data = measure.data(measure.states,:);
% simulate missing data
measure.data(1,15:27) = NaN;

nmeas = size(measure.data, 1);
measure.weight = eye(nmeas);
measure.alpha = 0.95;

%% estimate the parameters
estimates = parest(model, measure, objective);
disp('Estimated Parameters and Bounding Box')
[estimates.parest estimates.bbox]
estimates
%%plot the model fit to the noisy measurements
figure(1)
plot(model.tplot, estimates.x, measure.time, measure.data, 'o');
%title('Plot estimated functions for tplot')
figure(2)
plot(estimates.meastime, estimates.measpred, measure.time, measure.data, 'o');
%title('Plot estimated functions for measurement points')

% %% The code below here is for checking the sensitivity/adjoint gradient
% %% against finite difference. This part can be removed when code is working.
% function [numgrad, likegrad] = gradcheck(per, theta0)
%   global obj_
%   %%checking the gradient calculation
%   phi0 = likelihood(theta0);
%   likegrad = gradi(theta0);
%   numgrad = zeros(length(theta0), 1);
%   for i = 1: length(theta0)
%     theta = theta0;
%     theta(i) = (1+per)*theta(i);
%     phi = likelihood(theta);
%     numgrad(i) = (phi - phi0)/(per*theta(i));
%   endfor
% endfunction

% per = 1e-5;
% theta0 = objective.paric;
% trind =  obj_.logtr;
% theta0(trind) = log(theta0(trind));

% [numgrad, likegrad] = gradcheck(per, theta0);
% [numgrad, likegrad]
% diff = numgrad-likegrad;
% reldiff = norm(diff./likegrad)
% %% end of gradient check