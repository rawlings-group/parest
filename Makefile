TOPDIR := $(shell pwd)

ONE_FILE := parest_matlab_compatible.m	

SOURCE_FILES := \
	parest.m
# 	cvode_sens.m \
# 	likelihood.m \
# 	likelihoodA.m \
# 	sensrhsA.m \
# 	adjrhs.m

all: $(ONE_FILE)
.PHONY: all

$(ONE_FILE) : $(SOURCE_FILES)
	cat $(SOURCE_FILES) | \
	sed -e "s/endif/end/" \
	-e "s/endfor/end/" \
	-e "s/endwhile/end/" \
	-e "s/endswitch/end/" \
	-e "s/endfunction/end/" > \
	$@

clean:
	rm -f $(ONE_FILE)
.PHONY: clean
