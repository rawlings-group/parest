
global theta

part = "b";
switch (part)
  case {"a"}
    %% Exercise 9.11, part a, using only first experiment
    ca0ac = [2];
    cb0ac = [4];
    model.stateic_par = [1; 2];
  case {"b"}
    %% Exercise 9.11, part b, using only second experiment
    ca0ac = [5];
    cb0ac = [3];
    model.stateic_par = [1; 2];
  case{"c"}
    %% Exercise 9.11, part c, using both experiments
    ca0ac = [2; 5];
    cb0ac = [4; 3];
    model.stateic_par = [1, 2; 3, 4];
  otherwise
    error ("invalid value")
endswitch

nsets = length(ca0ac);
kac   = 0.1;
nac   = 2;
mac   = 1;
thetaac = [ca0ac; cb0ac; kac; nac; mac];
np  = length(thetaac);
ns  = 2;
tfinal = 5;
nts    = 100;
tout = linspace(0,tfinal,nts)';
%%
%% set seed for "reproducible" random numbers
%%
randn('seed',0);

% can be compiled for faster execution
function xdot = react2massbal(x, t, theta)
  ca      = x(1);
  cb      = x(2);
  k       = theta(end-2);
  n       = theta(end-1);
  m       = theta(end);
  if ( (ca <= 0.0) || (cb <= 0.0) )
    xdot = [0; 0];
  else
    xdot = [-k*ca^n*cb^m; -k*ca^n*cb^m];
  endif
endfunction

function xdot = react2massbal_wrap(t, x)
  global theta
  xdot = react2massbal(x, t, theta);
endfunction


%% solve model with true parameters to create measurement data
theta = thetaac;
yac = [];
for i = 1: nsets
  x0  = [ca0ac(i); cb0ac(i)];
  [tout, x] = ode15s(@react2massbal_wrap, tout, x0);
  yac = [yac, x];
endfor
%% add measurement noise
measvar = 1e-2;
measstddev = sqrt(measvar);
noise = measstddev*randn(nts,2*nsets);
ynoisy = yac + noise;

if (nsets > 1)
  tmp = reshape(ynoisy, nts, nsets, 2);
  measure.data = permute (tmp, [2, 1, 3]);
else
  measure.data = ynoisy';
endif
measure.time = kron(ones(1,nsets), tout);
measure.states = [1, 2];

model.odefcn = @react2massbal;
model.ic = [ca0ac'; cb0ac'];
model.param = thetaac;
np = length(model.param);

objective.estflag = [1:2*nsets+3];
objective.paric   = 1.1*thetaac;
objective.parlb   = 0.5*thetaac;
objective.parub   = 1.5*thetaac;
objective.sensitivityAdjointMethod = false;

% estimate parameters
ctime = cputime();
estimates = parest(model, measure, objective);
ctime = cputime()-ctime


disp ("The estimated parameters and bounding box")
[estimates.parest, estimates.bbox]

data = [measure.time,  ynoisy];
if (~ strcmp (getenv ('OMIT_PLOTS'), 'true')) %% PLOTTING
  for i = 1:nsets
    figure(i);
    col = i*nsets + 1;
    plot (data(:,i), data(:,[col,col+1]), '+', measure.time(:,i), estimates.measpred(:,:,i));
  endfor
endif %% PLOTTING

%% Copyright (C) 2001, James B. Rawlings and John G. Ekerdt
%%
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation; either version 2, or (at
%% your option) any later version.
%%
%% This program is distributed in the hope that it will be useful, but
%% WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program; see the file COPYING.  If not, write to
%% the Free Software Foundation, 59 Temple Place - Suite 330, Boston,
%% MA 02111-1307, USA.

%  %% Revised 7/22/02
%  %% Created 7/12/02
%  clear model measure objective
%
%  global theta_global
%
%  time = linspace(0,10,100)';
%  y01 = [2; 3];
%  y02 = [1;2];
%  theta_global =[1.5 0.05]; %n,k
%
%  function xdot = rxrate(x, t, theta)
%    ca = x(1);
%    n = theta(1);
%    k = theta(2);
%    if (ca <= 0.0)
%      xdot = [0;0];
%    else
%      xdot = [-k*ca^n; -k*x(2)+3];
%    end
%  endfunction
%
%  function xdot = drdx(x, t, theta)
%    ca = x(1);
%    n = theta(1);
%    k = theta(2);
%    if (ca <= 0.0)
%      xdot = 0;
%    else
%      xdot = [-k*n*ca^(n-1) 0;0 -k];
%    end
%  endfunction
%
%  function xdot = drdp(x, t, theta)
%    ca = x(1);
%    n = theta(1);
%    k = theta(2);
%    if (ca <= 0.0)
%      xdot = [0,0; 0 0];
%    else
%      xdot = [-k*log(ca)*ca^(n), -ca^n; 0, -x(2)];
%    end
%  endfunction
%
%  function retval = rxrate_fixme(t, x)
%    global theta_global
%    retval = rxrate(x, t, theta_global);
%  endfunction
%
%  % generate some data
%  [tout, ymeas1] = ode15s(@rxrate_fixme,  time, y01);
%  [tout, ymeas2] = ode15s(@rxrate_fixme,  time, y02);
%
%  ymeas1 = ymeas1 + 0.0005*randn(size(ymeas1));
%  ymeas2 = ymeas2 + 0.0005*randn(size(ymeas2));
%
%  %% PART C
%  %% nonlinear parameter estimation
%  model.odefcn = @rxrate;
%  %model.dodedp  =@drdp;
%  %model.dodedx = @drdx;
%  model.ic = [y01, y02];
%  model.param = theta_global;
%
%  measure.time = [time,time];
%  measure.data(:,:,1) = ymeas1';
%  measure.data(:,:,2) = ymeas2';
%  measure.states = [1 2];
%
%  %objective.estflag = [1 2];
%  objective.logtr = [1 0];
%  small = 1e-5;
%  large  = 5;
%  objective.parlb = [small; small];
%  objective.parub = [large; large];
%  % true parameters are theta_global
%  objective.paric = [1.5; .05];
%  objective.paric = [4.5; 0.25]; %XXX
%  %objective.sensitivityAdjointMethod = true;
%  estimates = parest(model, measure, objective);
%  ca = estimates.x;
%
%  %%echo results
%  theta_global
%  n = estimates.parest(1)
%  k = estimates.parest(2)
%  box = estimates.bbox
%  obj = estimates.obj
%
%  figure(1)
%  plot (time, ymeas1, '+', time,  ca(:,:,1),'g');
%  hold off
%  figure(2)
%  plot (time, ymeas2, '+', time,  ca(:,:,2),'g');
%
